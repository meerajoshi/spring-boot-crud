package com.ignek.helloworld.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ignek.helloworld.entity.Student;

public interface StudentRepository extends JpaRepository<Student,Integer>{
	
}
