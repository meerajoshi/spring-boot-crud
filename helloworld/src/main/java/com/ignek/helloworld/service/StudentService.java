package com.ignek.helloworld.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ignek.helloworld.entity.Student;
import com.ignek.helloworld.repository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository repository; 
	
	public Student saveStudent(Student student) {
		return repository.save(student); 
	}
	
	public List<Student> allStudent() {
		return repository.findAll();
	}
	public Student getStudentById(int i) {
		return repository.findById(i).orElse(null);
	}
	public void deleteStudent(int i) {
		repository.deleteById(i);
	}
	public Student UpdateStudent(Student student) {
		Student std=repository.findById(student.getId()).orElse(null);
		std.setName(student.getName());
		std.setEmail(student.getEmail());
		std.setEnrollmentNo(student.getEnrollmentNo());
		std.setContactNo(student.getContactNo());
		return repository.save(std);
	}
	
	
}
