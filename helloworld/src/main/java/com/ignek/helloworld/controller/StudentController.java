package com.ignek.helloworld.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ignek.helloworld.entity.Student;
import com.ignek.helloworld.service.StudentService;

@RestController
public class StudentController {
	@Autowired
	private StudentService service;
	
	@PostMapping("/addStudent")	
	public Student addStudent(@RequestBody Student student) {
		return service.saveStudent(student);
	}
	@GetMapping("/viewAllStudent")
	public List<Student> viewStudents(){
		return service.allStudent();
	}
	@GetMapping("/viewStudent/{id}")
	public Student viewStudent(@PathVariable int id){
		return service.getStudentById(id);
	}
	@GetMapping("/deleteStudent/{id}")
	public String deleteStudent(@PathVariable int id){
		String message="No Student Foud !!";
		Student std=service.getStudentById(id);
		if(std!=null) {
			service.deleteStudent(id);
			message="Student Deleted !!";
		}
		return message;
	}
	@PutMapping("/editStudent")	
	public Student UpdateStudent(@RequestBody Student student) {
		return service.saveStudent(student);
	}
}
