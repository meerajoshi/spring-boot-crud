package com.ignek.helloworld;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
public class HelloWorldController {

	@RequestMapping("/helloworldnew")
	@ResponseBody
	public String index(@RequestParam String id) {
		return "You entered id : "+id;
	}
	@RequestMapping("/getid")
	@ResponseBody
	public String getID(@RequestParam(name = "id") String id2, @RequestParam(defaultValue = "test") String name) { 
	    return "ID: " + id2 + "\nName: " + name;
	}
}